# Description

# Installation

This repository requires python 3.6 or higher.

## Developer Install

If you are working on the development of this repository or
are a client looking for more frequent updates, you may wish
to install the package using the instructions given below.

The dependencies for this repository may be installed by executing
`pip3 install -r requirements.txt`.  If you are contributing you
will want to install the pre-commit hooks found in the
[pre-commit config](./pre-commit-config.yaml) by executing
`pre-commit install`.

After installing dependencies, you must place the repository's
src/ directory on your $PYTHONPATH.  From the base of this
repository, this may be accomplished with the following command:

```
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

Finally, you will need to clone the
[kitchensink](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink)
and [code-predictor](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/code-predictor)
repositories, install their requirements, and place the src/ directories
on your $PYTHONPATH.  The kitchensink and code-predictor repositories are
dependencies of this repository under current development.  This setup
may be done with the following sequence of commands, to be run from the
parent directory of this repository:

```
git clone git@gitlab.com:GrammaTech/Mnemosyne/learn-synth/kitchensink.git
cd kitchensink
pip3 install -r requirements.txt
export PYTHONPATH=$PWD/src:$PYTHONPATH
cd -
git clone git@gitlab.com:GrammaTech/Mnemosyne/learn-synth/code-predictor.git
cd code-predictor
pip3 install -r requirements.txt
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

## Client Install

If you are using this library solely as a client, you may
create a python wheel file from this repository and install
it by executing the following sequence of commands:

```
python3 setup.py bdist_wheel --universal
pip3 install dist/*
```

Once installed, an `argument-predictor-evaluate` command will be
added to your $PATH which may be invoked directly using the same
interface and options described in the usage section below.

# Usage

The `evaluate.py` script in `src/argument_predictor_evaluate/cmd` offers
a method of evaluating the performance of various prediction engines
on function call argument prediction.  The evaluation script takes a
directory of programs (corpus) and compares the predictions generated
for each callarg versus the actual result.  Information on the precision
and mean recipricol rank (MRR) is output in a JSON format after performing
the evaluation.  An example invocation is given below:

```
python3 evaluate.py /path/to/data/corpus
```

As with the base argument prediction server, the client may adjust
the number of results returned (`--top-k`) by the prediction engine.
The prediction engines to utilize in evaluation may also be specified (`--engines`).
Additionally, the client may adjust the number of threads (`--threads`),
the number of files used in evaluation (`--num-files`) and print the
statistics in a "pretty-print" format instead of JSON (`--pretty-print`).
Finally, for advanced users, the client may pass in a JSON-formatted
dictionary of engine-specific arguments with (`--kwargs`).
