import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="code-predictor-evaluation",
    author="Grammatech",
    description="Evaluation framework to measure the efficacy of our code prediction tooling.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/code-predictor-evaluation",
    packages=["argument_predictor_evaluation.cmd"],
    package_dir={"": "src"},
    python_requires=">=3.8",
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    entry_points={
        "console_scripts": [
            "argument-predictor-evaluation = argument_predictor_evaluation.cmd.evaluate:main",
        ],
    },
    install_requires=[
        "jedi",
        "pathos",
        "psutil",
        "tqdm",
        "kitchensink @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink@master",
        "code-predictor @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/code-predictor@master",
    ],
    extras_require={"dev": ["black", "flake8", "pre-commit"]},
)
