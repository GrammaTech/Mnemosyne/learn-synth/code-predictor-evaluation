"""
Evaluation framework for probabilistic callarg prediction.
"""

import argparse
import atexit
import logging
import random
import re
import subprocess

from abc import abstractmethod
from collections import OrderedDict, defaultdict, namedtuple
from functools import lru_cache
from json import dumps, loads
from multiprocessing import Lock
from pathlib import Path
from shutil import which
from sys import stdout, maxsize
from time import sleep
from typing import Dict, List, Optional, Text, Tuple

from jedi import Script
from jedi.api.classes import Signature
from parso.python.tree import BaseNode
from pathos.pools import ProcessPool
from psutil import process_iter, virtual_memory
from tqdm import tqdm

from kitchensink.utility.parso import (
    ancestor_satisfying_predicate,
    callsite_signature,
    callsite_function,
    callsite_module,
    callargs,
    function_callsites,
    get_keyword,
    get_field,
    in_scope_names,
    is_constant,
    is_function_callsite,
    is_field_access,
    is_keyword_argument,
    normalize_quotes,
)
from kitchensink.utility.filesystem import python_files, safe_slurp
from kitchensink.utility.splitter import PythonRoninSplitter
from argument_predictor.statistical.predictor import Predictor
from argument_predictor.statistical.resolver import Function, PredictionContext
from argument_predictor.statistical.statistics import StatisticsDB, ZODBStatisticsDB

DEFAULT_ENGINE_CHOICES = ["ArgumentPredictor", "TabNine", "Galois", "Kite"]
DEFAULT_ENGINE = "ArgumentPredictor"
DEFAULT_TOP_K = 10
DEFAULT_THRESHOLD = 0.04
DEFAULT_SEED = 0


# simple data class representing a single prediction result, including
# the position in the file where the prediction took place and the rank
PredictionResult = namedtuple("PredictionResult", ["position", "rank"])


class Statistics:
    """
    represents aggregated statistics such as mean reciprocal rank and precision
    over a corpus of evaluated programs
    """

    def __init__(self, title: str, evaluation_results: List[Dict]):
        """
        compute and populate statistics from the given list of prediction
        evaluator results

        :param title: title to describe the prediction results
        :param evaluation_results: list of prediction engine evaluator results
        """

        def _precision1(ranks: List[int]) -> float:
            """
            return the precision@1 (first prediction was correct) / (total number
            of data points predicted) from the given list of @ranks

            :param ranks: list of ranks where rank is the position in the
                ranked list of predictions that is correct
            """
            count = len(ranks)
            if count > 0:
                return len([rank for rank in ranks if rank == 1]) / count
            else:
                return 0.0

        def _precision(ranks: List[int]) -> float:
            """
            return the overall precision (any prediction was correct) / (total
            number of data points predicted) from the given list of ranks

            :param ranks: list of ranks where rank is the position in the
                ranked list of predictions that is correct
            """
            count = len(ranks)
            if count > 0:
                return len([rank for rank in ranks if rank != maxsize]) / count
            else:
                return 0.0

        def _mrr(ranks: List[int]) -> float:
            """
            return the mean reciprocal rank (average over 1/rank for each
            prediction).

            :param ranks: list of ranks where rank is the position in the
                ranked list of predictions that is correct
            """
            count = len(ranks)
            if count > 0:
                return sum(1 / rank for rank in ranks) / count
            else:
                return 0.0

        def _populate_stats(stats: Dict, ranks: List[int]) -> Dict:
            """
            populate the given dictionary of @stats from the given
            list of @ranks, including the precision@1 and mean
            reciprocal rank.

            :param stats: dictionary of statistics to populate
            :param ranks: list of ranks where rank is the position in the
                ranked list of predictions that is correct
            """
            stats["count"] = len(ranks)
            stats["precision@1"] = _precision1(ranks)
            stats["precision"] = _precision(ranks)
            stats["mrr"] = _mrr(ranks)

        identifier_ranks = []
        constant_ranks = []
        for evaluation_result in evaluation_results:
            for prediction_result in evaluation_result["identifier_predictions"]:
                identifier_ranks.append(prediction_result.rank)
            for prediction_result in evaluation_result["constant_predictions"]:
                constant_ranks.append(prediction_result.rank)
        ranks = identifier_ranks + constant_ranks

        self.stats = defaultdict(dict)
        self.stats["title"] = title
        _populate_stats(self.stats["identifiers"], identifier_ranks)
        _populate_stats(self.stats["constants"], constant_ranks)
        _populate_stats(self.stats, ranks)

    def json(self, stream=stdout) -> None:
        """
        dump statistics from @self to @stream in JSON format.

        :param stream: output stream
        """
        print(dumps(self.stats), file=stream)

    def pretty_print(self, stream=stdout) -> None:
        """
        pretty print statistics from @self to @stream.

        :param stream: output stream
        """

        def _pretty_print_helper(stats: Dict, desc: Text) -> None:
            """
            Pretty-print the given dictionary of @stats with a leading
            header of @desc.

            :param stats: dictionary containing statistics such as precision
                 and mean reciprocal rank
            :param desc: header description for the statistics to be printed
            """
            print("\t%s" % desc, file=stream)
            print("\t\tTotal predictions: %d" % stats["count"], file=stream)
            print("\t\tPrecision @1: %.2f" % stats["precision@1"], file=stream)
            print("\t\tPrecision Overall: %.2f" % stats["precision"], file=stream)
            print("\t\tMRR: %.2f" % stats["mrr"], file=stream)

        print(self.stats["title"], file=stream)
        _pretty_print_helper(self.stats["identifiers"], "Identifiers")
        _pretty_print_helper(self.stats["constants"], "Constants")
        _pretty_print_helper(self.stats, "All")


class PredictionEngineEvaluator:
    """
    base class for all prediction engine evaluators
    """

    def __init__(self, path: Path, top_k: int = DEFAULT_TOP_K):
        """
        common initialization for prediction engine evaluator instances

        :param path: path to the python file to evaluate prediction against
        :param top_k: maximum number of prediction results to use in evaluation
        """
        assert path.exists(), f"{path} does not exist"
        assert top_k > 0, "top_k parameter must be greater than 0."

        self._script = Script(safe_slurp(path), path=path)
        self._top_k = top_k

    @property
    def script(self) -> Script:
        """
        return the python script associated with this prediction engine evaluator
        """
        return self._script

    @property
    def top_k(self) -> int:
        """
        return the maximum number of prediction results to use in evaluation
        """
        return self._top_k

    def evaluate(self) -> Dict:
        """
        evaluate the performance of the prediction engine on identifier and
        constant callargs in the python file associated with this evaluator,
        returning a dictonary with the path to the file and prediction
        results for identifiers and constants
        """

        def pop_keyword(callarg: BaseNode) -> Tuple[Optional[str], BaseNode]:
            """
            return the keyword from callarg (if one exists) and the callarg
            with the keyword dropped

            :param callarg: function call argument
            """
            if is_keyword_argument(callarg):
                keyword = get_keyword(callarg).value
                callarg = callarg.children[-1]
            else:
                keyword = None

            return keyword, callarg

        identifier_predictions = []
        constant_predictions = []

        root = self.script._module_node
        callsites = function_callsites(root)

        for callsite in callsites:
            for position, callarg in enumerate(callargs(callsite)):
                # Pop any keyword (e.g. `reverse=`) from the callarg.
                keyword, callarg = pop_keyword(callarg)

                if (
                    callarg.type == "name"
                    or is_field_access(callarg)
                    or is_constant(callarg)
                ):
                    # Perform the evaluation by comparing ground truth
                    # against the results from the prediction engine.
                    results = self.predict(callarg, position, keyword)
                    actual = normalize_quotes(callarg.get_code(include_prefix=False))

                    if results:
                        # Find the rank, where rank is the position in the
                        # prediction results of the ground truth, if it exists.
                        try:
                            rank = results.index(actual) + 1
                        except ValueError:
                            rank = maxsize

                        prediction_result = PredictionResult(
                            position=callarg.start_pos,
                            rank=rank,
                        )
                        if callarg.type == "name" or is_field_access(callarg):
                            identifier_predictions.append(prediction_result)
                        else:
                            constant_predictions.append(prediction_result)

        return {
            "path": self.script.path,
            "identifier_predictions": identifier_predictions,
            "constant_predictions": constant_predictions,
        }

    @abstractmethod
    def predict(
        self,
        callarg: BaseNode,
        position: int,
        keyword: Optional[str],
    ) -> List[Text]:
        """
        generate a list of top_k predictions for the given @callarg

        :param callarg: location to generate predictions for
        :param position: position of the callarg in the function call
        :param keyword: keyword argument given in the function call, if any
        """
        raise NotImplementedError()

    # Helper utilities for prediction engine evaluators

    @staticmethod
    @lru_cache(maxsize=128)
    def cached_lines(text: Text) -> List[Text]:
        """
        Return the lines in text as a list.

        :param text: source text of the program
        """
        return text.splitlines()

    @staticmethod
    def cached_subtext(text: Text, line: int, col: int) -> Text:
        """
        Return all characters in TEXT prior to line and col.

        :param text: source text of the program
        :param line: end line of the substring (one-indexed)
        :param col: end column of the substring (zero-indexed)
        :return substring of text prior to position
        """
        lines = PredictionEngineEvaluator.cached_lines(text)

        result = lines[: line - 1]
        if 0 < line <= len(lines):
            result.append(lines[line - 1][:col])
        result = "\n".join(result)
        return result

    _NEXT_ARGUMENT_REGEX = re.compile("^[ \t,]*([^ \t,]+)([ \t,]*.*)?$")

    @staticmethod
    def normalize_prediction(prediction: Text) -> Text:
        """
        normalize the prediction, removing trailing items beyond the
        next argument, removing extraneous paranethesis, and using
        double quotes where possible
        """
        m = re.search(
            PredictionEngineEvaluator._NEXT_ARGUMENT_REGEX,
            prediction.strip(),
        )
        result = m.group(1) if m else prediction.strip()
        result = normalize_quotes(result)
        if result.endswith("("):
            result = result.rstrip("(")
        if result.count(")") > result.count("(") and result.endswith(")"):
            result = result[:-1]
        return result


class ArgumentPredictorEvaluator(PredictionEngineEvaluator):
    """
    specialization of the prediction engine evaluator interface for our internal
    argument prediction tool
    """

    def __init__(
        self,
        path: Path,
        top_k: int = DEFAULT_TOP_K,
        threshold: float = DEFAULT_THRESHOLD,
        database: StatisticsDB = ZODBStatisticsDB(),
    ):
        """
        initialize an argument prediction evaluator instance

        :param path: path to the python file to evaluate prediction against
        :param top_k: maximum number of prediction results to use in evaluation
        :param threshold: minimum score (between [0.0, 1.0]) for a prediction
        :param database: statistics database of terms/constants collected from
            a corpus of programs
        """
        super().__init__(path, top_k)

        assert (
            0.0 <= threshold <= 1.0
        ), "threshold parameter must be between 0.0 and 1.0"
        self._threshold = threshold
        self._predictor = Predictor(database, PythonRoninSplitter())

    def predict(
        self,
        callarg: BaseNode,
        position: int,
        keyword: Optional[str],
    ) -> List[Text]:
        def signature_params(signature: Signature) -> List[str]:
            """
            return a list of parameter names from the given function @signature

            :param signature: function signature
            :return: list of a parameter names, if applicable
            """
            try:
                return [param.name for param in signature.params]
            except Exception:
                # Jedi may throw errors while inferring parameter names
                # which we catch and ignore.
                return []

        callsite = ancestor_satisfying_predicate(is_function_callsite, callarg)
        names = in_scope_names(callsite)
        signature = callsite_signature(self.script, callsite)
        function_name = callsite_function(self.script, callsite)
        module = callsite_module(self.script, callsite)
        params = signature_params(signature) if signature else []

        function = Function(module, function_name, params)
        context = PredictionContext(function, position, keyword, names)

        return self._predictor.predict_argument(context, self._top_k, self._threshold)


class TabNineEvaluator(PredictionEngineEvaluator):
    """
    specialization of the prediction engine evaluator interface for TabNine
    """

    _CMD = "TabNine"
    _VERSION = "3.4.18"
    _CHAR_LIMIT = 100000  # from tabnine-vscode:consts.ts

    def __init__(
        self,
        path: Path,
        top_k: int = DEFAULT_TOP_K,
    ):
        """
        initialize a TabNine evaluator instance

        :param path: path to the python file to evaluate prediction against
        :param top_k: maximum number of prediction results to use in evaluation
        """
        super().__init__(path, top_k)

        # Spawn the TabNine process
        assert which(
            TabNineEvaluator._CMD
        ), f"{TabNineEvaluator._CMD} must be on your $PATH"
        self._proc = subprocess.Popen(
            [TabNineEvaluator._CMD],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        # Add the file to TabNine's index.
        args = defaultdict(lambda: defaultdict(dict))
        args["version"] = TabNineEvaluator._VERSION
        args["request"]["Prefetch"]["filename"] = str(path)
        self._proc.stdin.write(dumps(args).encode("ascii"))
        self._proc.stdin.write(b"\n")
        self._proc.stdin.flush()
        self._proc.stdout.readline()

    def __del__(self):
        """
        terminate the TabNine process upon object finalization
        """
        if hasattr(self, "_proc"):
            self._proc.kill()

    def predict(
        self,
        callarg: BaseNode,
        position: int,
        keyword: Optional[str],
    ) -> List[Text]:

        # Build an autocomplete request and send it to the TabNine process.
        before_text = PredictionEngineEvaluator.cached_subtext(
            self.script._module_node.get_code(),
            *callarg.start_pos,
        )
        includes_begin = len(before_text) < TabNineEvaluator._CHAR_LIMIT
        before_text = (
            before_text[-TabNineEvaluator._CHAR_LIMIT :]
            if not includes_begin
            else before_text
        )
        args = defaultdict(lambda: defaultdict(dict))
        args["version"] = TabNineEvaluator._VERSION
        args["request"]["Autocomplete"]["filename"] = str(self.script.path)
        args["request"]["Autocomplete"]["before"] = before_text
        args["request"]["Autocomplete"]["after"] = ""
        args["request"]["Autocomplete"]["region_includes_beginning"] = includes_begin
        args["request"]["Autocomplete"]["region_includes_end"] = False
        args["request"]["Autocomplete"]["max_num_results"] = self._top_k
        self._proc.stdin.write(dumps(args).encode("ascii"))
        self._proc.stdin.write(b"\n")
        self._proc.stdin.flush()

        # Read the response from TabNine and return the prediction results.
        try:
            response = self._proc.stdout.readline().decode("ascii").strip()
            response = loads(response) if response else {}
            response = response if response else {}
        except UnicodeDecodeError:
            response = {}

        results = []
        if stdout:
            for result in response.get("results", []):
                result = result["new_prefix"]
                result = PredictionEngineEvaluator.normalize_prediction(result)
                results.append(result)
        results = list(OrderedDict.fromkeys(results))

        return results


class GaloisEvaluator(PredictionEngineEvaluator):
    """
    specialization of the prediction engine evaluator interface for the
    open-source Galois project
    """

    # Start docker image on the host with
    # docker run \
    #        --rm
    #        --network=host \
    #        --gpus all \
    #        registry.gitlab.com/grammatech/mnemosyne/argot-server/autocomplete
    #        ./autocomplete --rest

    _HOST = "idas-gpu-01"
    _PORT = 3030
    _URI = "autocomplete"
    _CHAR_LIMIT = 5000

    def __init__(
        self,
        path: Path,
        top_k: int = DEFAULT_TOP_K,
    ):
        """
        initialize a Galois evaluator instance

        :param path: path to the python file to evaluate prediction against
        :param top_k: maximum number of prediction results to use in evaluation
        """
        super().__init__(path, top_k)

        # Check if the REST API can be invoked properly.
        assert (
            subprocess.Popen(
                GaloisEvaluator.rest_command(""),
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            ).wait()
            == 0
        ), f"Ensure Galois autocomplete is running on {GaloisEvaluator._HOST}:{GaloisEvaluator._PORT}"

    def predict(
        self,
        callarg: BaseNode,
        position: int,
        keyword: Optional[str],
    ) -> List[Text]:
        # Build an autocomplete request and send it to the Galois process.
        subtext = PredictionEngineEvaluator.cached_subtext(
            self.script._module_node.get_code(),
            *callarg.start_pos,
        )
        proc = subprocess.Popen(
            GaloisEvaluator.rest_command(subtext),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = proc.communicate()

        # Read the response from Galois and return the prediction results.
        response = loads(stdout) if stdout.strip() and proc.poll() == 0 else {}
        response = response if response else {}

        results = []
        for result in response.get("result", []):
            result = PredictionEngineEvaluator.normalize_prediction(result)
            results.append(result)
        results = list(OrderedDict.fromkeys(results))
        results = results[: self.top_k]

        return results

    @staticmethod
    def rest_command(text: Text) -> List[str]:
        """
        Build an autocompletion request for the Galois autocomplete REST API
        with the given leading @text.

        :param text: text prior to the completion request point
        """
        return [
            "curl",
            "-X",
            "POST",
            f"http://{GaloisEvaluator._HOST}:{GaloisEvaluator._PORT}/{GaloisEvaluator._URI}",
            "-H",
            "'Content-Type: application/json'",
            "-d",
            dumps({"text": text[: GaloisEvaluator._CHAR_LIMIT]}),
        ]


class KiteDaemon:
    """
    singleton for managing the Kite daemon process
    """

    _START_CMD = "kited"
    _STOP_CMD = "pkill"
    _proc = None
    _lock = Lock()

    @staticmethod
    def is_running() -> bool:
        """
        return TRUE if the Kite daemon subprocess is running
        """
        for proc in process_iter():
            try:
                if KiteDaemon._START_CMD in proc.name().lower() and proc.status() in [
                    "running",
                    "sleeping",
                ]:
                    return True
            except Exception:
                pass

        return False

    @staticmethod
    def start() -> None:
        """
        start the Kite daemon subprocess
        """
        with KiteDaemon._lock:
            if not KiteDaemon.is_running():
                # spawn the daemon process
                assert which(
                    KiteDaemon._START_CMD
                ), f"{KiteDaemon._START_CMD} must be on your $PATH."
                assert which(
                    KiteDaemon._STOP_CMD
                ), f"{KiteDaemon._STOP_CMD} must be on your $PATH."
                KiteDaemon._proc = subprocess.Popen(
                    [
                        KiteDaemon._START_CMD,
                        "--plugin-launch",
                        "--channel=autocomplete-python",
                    ],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )

                # check to see if tensorflow was loaded and give some
                # buffer time to initialize everything
                for line in KiteDaemon._proc.stderr:
                    if "loading tensorflow took" in line.decode("ascii").strip():
                        break
                sleep(5.0)

    @staticmethod
    def stop() -> None:
        """
        stop the Kite daemon subprocess
        """
        with KiteDaemon._lock:
            if KiteDaemon.is_running():
                subprocess.run([KiteDaemon._STOP_CMD, KiteDaemon._START_CMD])

    @staticmethod
    def check_memory_usage(threshold: float = 0.7) -> None:
        """
        check memory usage, restarting the Kite daemon if memory usage exceeds
        the given threshold proportion of total memory

        :param threshold: threshold proportion of total memory in use to force
            a restart of the Kite process
        """
        total_memory = virtual_memory().total
        used_memory = total_memory - virtual_memory().available

        if used_memory / total_memory > threshold:
            KiteDaemon.stop()
            KiteDaemon.start()


atexit.register(KiteDaemon.stop)


class KiteEvaluator(PredictionEngineEvaluator):
    """
    specialization of the prediction engine evaluator interface for Kite
    """

    # The `kited` process must be running on the local machine.

    _HOST = "localhost"
    _PORT = 46624
    _URI = "clientapi/editor/complete"
    _CHAR_LIMIT = 100000  # kite:max-file-size = 1024 kb, match TabNine

    def __init__(
        self,
        path: Path,
        top_k: int = DEFAULT_TOP_K,
    ):
        """
        initialize a Kite evaluator instance

        :param path: path to the python file to evaluate prediction against
        :param top_k: maximum number of prediction results to use in evaluation
        """
        super().__init__(path, top_k)

        # Check if the REST API can be invoked properly.
        KiteDaemon.start()
        assert (
            subprocess.Popen(
                KiteEvaluator.rest_command(path, text=""),
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            ).wait()
            == 0
        ), f"Ensure Kite is running on {KiteEvaluator._HOST}:{KiteEvaluator._PORT}"

    def predict(
        self,
        callarg: BaseNode,
        position: int,
        keyword: Optional[str],
    ) -> List[Text]:
        def destructure_name(callarg: BaseNode) -> Tuple[Optional[str], BaseNode]:
            """
            if the given @callarg is a dotted name, destructure it into its
            leading and field components

            :param callarg: function call argument
            """
            field = callarg
            while is_field_access(field):
                field = get_field(field)

            leading = callarg.get_code(False)[: -len(field.get_code(False))]
            return leading, field

        # Ensure the kite daemon's memory usage is reasonable and if not,
        # restart the process.
        KiteDaemon.check_memory_usage()

        # Kite will not predict field accesses until an name is
        # populated for the callarg.  To allow for a more fair
        # comparison, destructure the callarg into its leading and
        # field components and ask Kite to predict the field to be
        # used.
        leading, field = destructure_name(callarg)

        # Build an autocomplete request and send it to the Kite REST API.
        subtext = PredictionEngineEvaluator.cached_subtext(
            self.script._module_node.get_code(),
            *field.start_pos,
        )
        proc = subprocess.Popen(
            KiteEvaluator.rest_command(self.script.path, subtext),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = proc.communicate()

        # Read the response from Kite and return the prediction results.
        response = loads(stdout) if stdout.strip() and proc.poll() == 0 else {}
        response = response if response else {}
        response = response.get("completions", []) or []

        results = []
        for result in response:
            result = leading + result["display"]
            result = PredictionEngineEvaluator.normalize_prediction(result)
            results.append(result)
        results = list(OrderedDict.fromkeys(results))
        results = results[: self.top_k]

        return results

    @staticmethod
    def rest_command(path: Path, text: Text) -> List[str]:
        """
        Build a completion request for the Kite autocomplete REST API
        with the given leading @text from the file at @path.

        :param path: file we are processing
        :param text: text in file prior to the completion request point
        """
        request = {
            "text": text[: KiteEvaluator._CHAR_LIMIT],
            "editor": "vscode",
            "filename": str(path),
            "position": {
                "begin": len(text),
                "end": len(text),
            },
            "no_snippets": True,
            "offset_encoding": "utf-16",
        }

        return [
            "curl",
            "-X",
            "POST",
            f"http://{KiteEvaluator._HOST}:{KiteEvaluator._PORT}/{KiteEvaluator._URI}",
            "-H",
            "'Content-Type: application/json'",
            "-d",
            dumps(request),
        ]


class PredictionEngineEvaluatorFactory:
    """
    singleton factory for creating prediction engine evaluator instances
    """

    @staticmethod
    def create_evaluator(
        path: Path, engine: str, *args: List, **kwargs: Dict
    ) -> PredictionEngineEvaluator:
        """
        factory method for creating prediction engine evaluator instances

        :param path: path to the python file to evaluate prediction against
        :param engine: type of prediction engine to use in evaluation
        :param args: additional arguments to pass to evaluator creation
        :param kwargs: additional arguments to pass to evaluator creation
        """
        if engine.lower() == "argumentpredictor":
            return ArgumentPredictorEvaluator(path, *args, **kwargs)
        elif engine.lower() == "tabnine":
            return TabNineEvaluator(path, *args, **kwargs)
        elif engine.lower() == "galois":
            return GaloisEvaluator(path, *args, **kwargs)
        elif engine.lower() == "kite":
            return KiteEvaluator(path, *args, **kwargs)

        raise RuntimeError(f"{engine} is not a supported prediction engine.")


def common_results(
    results: List[Tuple[str, List[Dict]]]
) -> List[Tuple[str, List[Dict]]]:
    """
    return the prediction results common to all engines in @results

    :param results: results from the prediction evaluations
    """

    # Find those points which are common to all engine predictions.
    common_pts = defaultdict(list)
    for engine, evaluation_results in results:
        for evaluation_result in evaluation_results:
            path = evaluation_result["path"]
            identifier_pts = {
                prediction_result.position
                for prediction_result in evaluation_result["identifier_predictions"]
            }
            constant_pts = {
                prediction_result.position
                for prediction_result in evaluation_result["constant_predictions"]
            }
            all_pts = identifier_pts.union(constant_pts)
            common_pts[path].append(all_pts)

    for path in common_pts:
        if len(common_pts[path]) != len(results):
            common_pts[path] = set()
        else:
            for i, pts in enumerate(common_pts[path]):
                if not i:
                    intersection = pts
                else:
                    intersection = pts.intersection(intersection)
            common_pts[path] = intersection

    # Filter the prediction results for each engine to only those common
    # points and return.
    common_predictions = []
    for engine, evaluation_results in results:
        common_evaluation_results = []

        for evaluation_result in evaluation_results:
            path = evaluation_result["path"]
            identifier_predictions = [
                prediction_result
                for prediction_result in evaluation_result["identifier_predictions"]
                if prediction_result.position in common_pts[path]
            ]
            constant_predictions = [
                prediction_result
                for prediction_result in evaluation_result["constant_predictions"]
                if prediction_result.position in common_pts[path]
            ]

            common_evaluation_results.append(
                {
                    "path": path,
                    "identifier_predictions": identifier_predictions,
                    "constant_predictions": constant_predictions,
                }
            )

        common_predictions.append((f"{engine} (common)", common_evaluation_results))

    return common_predictions


def main():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument(
        "corpus",
        type=Path,
        help="Directory with python files to evaluate argument prediction against.",
    )
    parser.add_argument(
        "--num-files",
        type=int,
        help="Maximum number of files in the corpus to evaluate.",
    )
    parser.add_argument(
        "--num-threads",
        type=int,
        default=1,
        help="Number of threads to utilize.",
    )
    parser.add_argument(
        "--pretty-print",
        action="store_true",
        help="Pretty print statistical output.",
    )
    parser.add_argument(
        "--engines",
        type=str,
        default=DEFAULT_ENGINE,
        help="Comma delimited list of prediction engines to evaluate.",
    )
    parser.add_argument(
        "--top-k",
        type=int,
        default=DEFAULT_TOP_K,
        help="Max number of code prediction results at each evaluation.",
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=DEFAULT_SEED,
        help="Random seed for the process.",
    )
    parser.add_argument(
        "--kwargs",
        type=str,
        default="{}",
        help="JSON formatted dictionary of additional prediction engine arguments.",
    )

    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
    args = parser.parse_args()
    random.seed(args.seed)
    args.corpus = args.corpus.resolve()
    args.kwargs = loads(args.kwargs)
    args.engines = [engine.strip() for engine in args.engines.split(",")]
    args.engines = [engine for engine in args.engines if engine]
    for engine in args.engines:
        assert (
            engine in DEFAULT_ENGINE_CHOICES
        ), f"{engine} is not one of {DEFAULT_ENGINE_CHOICES}"

    logging.info(f"Collecting python files in {str(args.corpus)}.")
    files = python_files(args.corpus)
    random.shuffle(files)
    files = files[: args.num_files]

    logging.info("Collecting argument prediction statistics on the corpus.")
    results = []
    for engine in args.engines:

        def driver(path: Path) -> Dict:
            """
            Driver returning prediction statistics for the file at @path.
            """
            evaluator = PredictionEngineEvaluatorFactory.create_evaluator(
                path=path,
                engine=engine,
                top_k=args.top_k,
                **args.kwargs,
            )
            evaluator_results = evaluator.evaluate()
            del evaluator

            return evaluator_results

        # Iterate over each file, collecting identifier and constant ranks
        # for each file into @ranks.  After the iteration is complete,
        # split out and flatten the ranks into separate lists.
        num_threads = args.num_threads if engine.lower() != "kite" else 1
        if num_threads > 1:
            with ProcessPool(args.num_threads) as pool:
                evaluation_results = list(
                    tqdm(pool.imap(driver, files), total=len(files))
                )
        else:
            evaluation_results = list(tqdm(map(driver, files), total=len(files)))

        # Stop the Kite daemon once we have finished evaluating that engine.
        if engine.lower() == "kite":
            KiteDaemon.stop()

        results.append((engine, evaluation_results))

    logging.info("Outputting collected statistics on predictions for each engine.")
    for engine, evaluation_results in results:
        stats = Statistics(engine, evaluation_results)
        if args.pretty_print:
            stats.pretty_print(stream=stdout)
        else:
            stats.json(stream=stdout)

    if len(args.engines) > 1:
        logging.info("Outputting collected statistics on common predictions.")
        for engine, evaluation_results in common_results(results):
            stats = Statistics(engine, evaluation_results)
            if args.pretty_print:
                stats.pretty_print(stream=stdout)
            else:
                stats.json(stream=stdout)


if __name__ == "__main__":
    main()
